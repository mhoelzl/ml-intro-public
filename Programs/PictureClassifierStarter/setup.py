from setuptools import setup, find_packages

setup(
    name='pictureclassifierstarter',
    version='0.1',
    packages=find_packages(),
    entry_points={'console_scripts': [
        'pictureclassifierstarter = pictureclassifierstarter.main:main',
        'pictureclassifiergui = pictureclassifierstarter.main:gui'
    ]},
    author='Matthias Hölzl',
    author_email='tc@xantira.com',
    description='A classifier for pictures',
    long_description='A program to test various classifiers for images in the MNIST format',
    long_description_content_type='text/markdown',
    url='https://gitlab.com/mhoelzl/ml-intro-cam',
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: ISC License (ISCL)',
        'Operating System :: OS Independent',
        'Development Status :: 3 - Alpha',
    ],
    python_requires='>=3.7',
    install_requires=[
        'tensorflow>=2.2',
        'Pillow>=7.1.2',
        'numpy~=1.18.5',
    ],
)

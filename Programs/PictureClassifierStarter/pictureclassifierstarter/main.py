import logging
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
logging.getLogger('tensorflow').setLevel(logging.FATAL)

import argparse
import numpy as np
from pathlib import Path
from .classifiers import Classifier
from .images import ImageStore, LabeledImage


## Command line arguments:
# > pictureclassifier.exe --help
# usage: pictureclassifier [-h] [-c CLASSIFIER] [-v] <img or dir> [<img or dir> ...]
#
# Classify images in MNIST format.
#
# positional arguments:
#   <img or dir>          path to image or folder containing images
#
# optional arguments:
#   -h, --help            show this help message and exit
#   -c CLASSIFIER, --classifier CLASSIFIER
#                         load a classifier mod

def get_args() -> argparse.Namespace:
    # TODO
    pass

def gui():
    print("Jetzt mit GUI>")

def main():
    """
    args = get_args()
    if not args.classifier:
        print("No classifier specified. Aborting.")
        return
    for classifier_str in args.classifier:
        Classifier.load(Path(classifier_str))
    images = list(map(Path, args.image))
    image_store = ImageStore()
    image_store.load_all_images(images)
    image: LabeledImage
    for image in image_store:
        print(f"Image: {image.filename}")
        for label in image.labels:
            with np.printoptions(precision=3, suppress=True):
                print(f"{label}: value = {np.argmax(image.labels[label])}", end='')
                if args.verbose:
                    print(f" weights = {image.labels[label]}")
                else:
                    print()
    """
    print("Hello from changed program")


if __name__ == '__main__':
    main()
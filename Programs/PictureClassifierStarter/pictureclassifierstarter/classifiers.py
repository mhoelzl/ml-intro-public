from abc import ABC, abstractmethod
from pathlib import Path
from PIL import ImageOps
from PIL.Image import Image as PilImage
from tensorflow.keras.models import Model
from typing import Dict, Any
import numpy as np
import tensorflow.keras.models as models


class Classifier(ABC):
    def __init__(self, name: str):
        # TODO: Add attribute
        pass

    @staticmethod
    def load(path: Path):  # returns a Classifier
        extension = path.suffix
        result: Classifier
        # TODO: implement
        # Attention: Make sure that `result` is added to _classifiers (see below)
        return result

    # noinspection PyMethodMayBeStatic
    def convert_image(self, image: PilImage) -> np.ndarray:
        image_resized = image.resize((28, 28), reducing_gap=3.0)
        image_gray = image_resized.convert('L')
        return np.array(ImageOps.invert(image_gray), dtype='float32') / 255

    @abstractmethod
    def predict(self, image: PilImage):
        pass


class TensorflowClassifier(Classifier):
    def __init__(self, name: str, model: Model):
        super().__init__(name)
        # TODO: Add attributes

    def convert_image(self, image: PilImage) -> np.ndarray:
        image_array = super().convert_image(image)
        # TODO: reshape result to correct size
        result = image_array
        return result

    def predict(self, image: PilImage):
        # TODO: implement
        return [0.0] * 10


class SklearnClassifier(Classifier):
    def __init__(self, name: str, model):
        super().__init__(name)
        # TODO: Add attributes

    def predict(self, image: PilImage):
        # TODO: implement
        return [0.0] * 10


_classifiers: Dict[str, Classifier] = {}


def get_all_classifiers() -> Dict[str, Classifier]:
    return _classifiers


def load_tensorflow_model(path: Path) -> Model:
    print(f"Loading tensorflow model {path.name}.")
    # TODO: implement
    model: Model = None
    return model


def load_sklearn_model(path) -> Any:
    print(f"Loading sklearn model {path.name}")
    return None

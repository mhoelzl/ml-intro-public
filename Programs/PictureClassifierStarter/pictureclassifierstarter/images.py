from collections import UserList
from PIL import Image
from PIL.Image import Image as PilImage
from pathlib import Path
from typing import Union, Dict, Iterable, List

from .classifiers import get_all_classifiers, Classifier


class LabeledImage:
    """"An image labeled by a number of classifiers."""

    def __init__(self, image: PilImage, labels: Dict[str, int]):
        # TODO: Add attributes
        pass

    @property
    def filename(self):
        return self.image.filename


def convert_to_labeled_image(image: Union[LabeledImage, PilImage]):
    if isinstance(image, PilImage):
        labels = apply_all_classifiers(image)
        return LabeledImage(image, labels)
    elif isinstance(image, LabeledImage):
        return image
    else:
        raise ValueError("Argument must either be an Image or a LabeledImage.")


def apply_all_classifiers(image: PilImage) -> Dict[str, int]:
    result: Dict[str, int] = {}
    # TODO
    return result


class ImageStore(UserList):
    """A store for labeled images."""

    def __init__(self, initlist=None):
        super().__init__(initlist)

    def load_all_images(self, files: List[Path]):
        # TODO
        pass

    def load_image(self, path: Path):
        # TODO
        pass

    def append(self, image: Union[LabeledImage, PilImage]):
        super().append(convert_to_labeled_image(image))

    def extend(self, other: Iterable[Union[LabeledImage, PilImage]]) -> None:
        super().extend(map(convert_to_labeled_image, other))

    def insert(self, i: int, item: Union[LabeledImage, PilImage]) -> None:
        super().insert(i, convert_to_labeled_image(item))

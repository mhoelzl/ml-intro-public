import logging
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
logging.getLogger('tensorflow').setLevel(logging.FATAL)

import argparse
import numpy as np
from pathlib import Path
from .classifiers import Classifier
from .images import ImageStore, LabeledImage


def get_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='Classify images in MNIST format.')
    parser.add_argument('-c', '--classifier',
                        action='append',
                        type=str,
                        help='load a classifier model')
    parser.add_argument('image',
                        nargs='+',
                        type=str,
                        metavar='<img or dir>',
                        help='path to image or folder containing images')
    parser.add_argument('-v', '--verbose',
                        action='store_const',
                        const=True,
                        default=False,
                        help='print verbose output')
    return parser.parse_args()


def main():
    args = get_args()
    if not args.classifier:
        print("No classifier specified. Aborting.")
        return
    for classifier_str in args.classifier:
        Classifier.load(Path(classifier_str))
    images = list(map(Path, args.image))
    image_store = ImageStore()
    image_store.load_all_images(images)
    image: LabeledImage
    for image in image_store:
        print(f"Image: {image.filename}")
        for label in image.labels:
            with np.printoptions(precision=3, suppress=True):
                print(f"{label}: value = {np.argmax(image.labels[label])}", end='')
                if args.verbose:
                    print(f" weights = {image.labels[label]}")
                else:
                    print()


if __name__ == '__main__':
    main()

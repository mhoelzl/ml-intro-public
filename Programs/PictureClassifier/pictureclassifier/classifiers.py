from abc import ABC, abstractmethod
from pathlib import Path
from PIL import ImageOps
from PIL.Image import Image as PilImage
from tensorflow.keras.models import Model
from typing import Dict, Any
import numpy as np
import tensorflow.keras.models as models


class Classifier(ABC):
    def __init__(self, name: str):
        self.name: str = name

    @staticmethod
    def load(path: Path):  # returns a Classifier
        extension = path.suffix
        result: Classifier
        if extension in ['.h5', '.tf']:
            model = load_tensorflow_model(path)
            result = TensorflowClassifier(path.name, model)
        elif extension in ['.pickle']:
            model = load_sklearn_model(path)
            result = SklearnClassifier(path.name, model)
        else:
            raise ValueError('File format not recognized.')
        _classifiers[path.name] = result
        return result

    # noinspection PyMethodMayBeStatic
    def convert_image(self, image: PilImage) -> np.ndarray:
        image_resized = image.resize((28, 28), reducing_gap=3.0)
        image_gray = image_resized.convert('L')
        return np.array(ImageOps.invert(image_gray), dtype='float32') / 255

    @abstractmethod
    def predict(self, image: PilImage):
        pass


class TensorflowClassifier(Classifier):
    def __init__(self, name: str, model: Model):
        super().__init__(name)
        self.model: Model = model

    def convert_image(self, image: PilImage) -> np.ndarray:
        image_array = super().convert_image(image)
        return image_array.reshape((1, 28, 28, 1))

    def predict(self, image: PilImage):
        classifier_image = self.convert_image(image)
        return self.model.predict(classifier_image)


class SklearnClassifier(Classifier):
    def __init__(self, name: str, model):
        super().__init__(name)
        self.model = model

    def predict(self, image: PilImage):
        classifier_image = self.convert_image(image)
        return [0.0] * 10
        # return self.model.predict(classifier_image)


_classifiers: Dict[str, Classifier] = {}


def get_all_classifiers() -> Dict[str, Classifier]:
    return _classifiers


def load_tensorflow_model(path: Path) -> Model:
    print(f"Loading tensorflow model {path.name}.")
    model: Model = models.load_model(path)
    return model


def load_sklearn_model(path) -> Any:
    print(f"Loading sklearn model {path.name}")
    return None
